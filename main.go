package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	"text/template"

	"github.com/AlecAivazis/survey"
)

type TemplateValues struct {
	Name        string
	Description string
	Command     string
	WorkDir     string
	After       string
	User        string
	Restart     string
	RestartSec  string
	WantedBy    string
}

func promt_values() TemplateValues {
	currentWorkingDirectory, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	user, err := user.Current()
	if err != nil {
		panic(err)
	}

	var qs = []*survey.Question{
		{
			Name:      "name",
			Prompt:    &survey.Input{Message: "How should the .service file be named? Do NOT include '.service'"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},
		{
			Name:     "description",
			Prompt:   &survey.Input{Message: "Describe your service very short (will appear in logs, e.g. Started DESCRIPTION)"},
			Validate: survey.Required,
		},
		{
			Name:     "command",
			Prompt:   &survey.Input{Message: "What command should be run when service starts?"},
			Validate: survey.Required,
		},
		{
			Name:     "workdir",
			Prompt:   &survey.Input{Message: "In which directory should it be executed?", Default: currentWorkingDirectory},
			Validate: survey.Required,
		},
		{
			Name:     "user",
			Prompt:   &survey.Input{Message: "As which user do you want to run the command?", Default: user.Username},
			Validate: survey.Required,
		},
		{
			Name:     "after",
			Prompt:   &survey.Input{Message: "On Startup, after which service/target do you want this to start?", Default: "multi-user.target"},
			Validate: survey.Required,
		},
		{
			Name: "restart",
			Prompt: &survey.Select{
				Message: "When do you want the service to restart?",
				Options: []string{"No", "On-success", "On-failure", "On-abnormal", "On-watchdog", "On-abort", "Always"},
				Default: "Always",
			},
		},
		{
			Name:   "restartsec",
			Prompt: &survey.Input{Message: "How long should systemd wait before restarting the service? (in seconds)", Default: "0"},
		},
		{
			Name:     "wantedby",
			Prompt:   &survey.Input{Message: "Which service/target wants your service?", Default: "multi-user.target"},
			Validate: survey.Required,
		},
	}
	values := TemplateValues{}

	err = survey.Ask(qs, &values)
	if err != nil {
		fmt.Println(err.Error())
		return values
	}

	return values
}

func main() {
	values := promt_values()

	tmpl, err := template.New("test").Parse(`
[Unit]
Description={{.Description}}
After={{.After}}

[Service]
Type=simple
WorkingDirectory={{.WorkDir}}
User={{.User}}
ExecStart={{.Command}}
Restart={{.Restart}}
RestartSec={{.RestartSec}}

[Install]
WantedBy={{.WantedBy}}`)
	if err != nil {
		panic(err)
	}

	f, err := os.Create(fmt.Sprintf("/etc/systemd/system/%s.service", values.Name))
	if err != nil {
		fmt.Println(err)
		return
	}

	err = tmpl.Execute(f, values)
	if err != nil {
		panic(err)
	}

	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	startNow := false
	prompt := &survey.Confirm{
		Message: "Do you want to start the service now?",
	}
	survey.AskOne(prompt, &startNow)

	enable := false
	prompt = &survey.Confirm{
		Message: "Do you want to enable the service now?",
	}
	survey.AskOne(prompt, &enable)

	err = exec.Command("systemctl", "daemon-reload").Run()
	if err != nil {
		panic(err)
	}
	if startNow == true {
		err = exec.Command("systemctl", "start", values.Name).Run()
	}
	if enable == true {
		err = exec.Command("systemctl", "enable", values.Name).Run()
	}
}
